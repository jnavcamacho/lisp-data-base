(define (main)
  (base_datos 2 null))

#lang scheme
(define (base_datos datos orden argumentos)
  (cond
    ((null? orden)(base_datos datos (format "~s"(read)) (format "~s"(read))))
    ((equal? "hola" orden)(
                          (print "hola mundo")
                          (base_datos datos null null)))
    ((equal? orden "Adios")
     (print "Gracias por utilizar la base de datos"))
    (else(
          (print "Error no se comprende la orden")
          (base_datos datos null null)))))
                       
                  
(define (main)
  (base_datos 2 null null))